#include <string>
#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_broadcaster.h>
#include <urdf/model.h>

double _steer;
void cbVelSteer(const geometry_msgs::TwistConstPtr& msg)
{
  _steer = msg->angular.z;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "joint_state_publisher");
  ros::NodeHandle nh;
  ros::Publisher joint_pub = nh.advertise<sensor_msgs::JointState>("joint_stat", 1);
  ros::Subscriber subVelSteer = nh.subscribe<geometry_msgs::Twist>("/vel_steer_info", 100, cbVelSteer);
  tf::TransformBroadcaster broadcaster;
  ros::Rate loop_rate(50);

  urdf::Model model;
  if(!model.initParam("robot_description")) {
    ROS_ERROR("fail loading model");
    return -1;
  }

  int jointCnt = model.joints_.size();
  ROS_WARN_STREAM("joint cnt: " << jointCnt);

  sensor_msgs::JointState joint_state;
  int i = 0;
  int frIdx = 0;
  int flIdx = 0;

  for(std::map<std::string, urdf::JointSharedPtr>::iterator iter = model.joints_.begin(); iter != model.joints_.end(); iter++)
  {
    std::string key = iter->first;
    ROS_INFO_STREAM("j : " << key);
    joint_state.name.push_back(key);
    joint_state.position.push_back(0);

    if(key.find("wheel_fr") != std::string::npos)
      frIdx = i;
    else if(key.find("wheel_fl") != std::string::npos)
      flIdx = i;
    ++i;
  }

  ROS_WARN_STREAM("fr : " << frIdx << ", fl : " << flIdx);

  while(ros::ok())
  {
    joint_state.header.stamp = ros::Time::now();
    joint_state.position[frIdx] = _steer;
    joint_state.position[flIdx] = _steer;

    joint_pub.publish(joint_state);
    loop_rate.sleep();
    ros::spinOnce();
  }

  return 0;
}
